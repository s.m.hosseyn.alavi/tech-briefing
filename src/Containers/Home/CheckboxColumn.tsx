import { Checkbox, Divider, FormControlLabel } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { CheckBox } from ".";

interface CheckboxColumnProps {
  title: string;
  checkboxes: CheckBox[];
  handleChange: (key: number, value: boolean, parentKey?: number) => void;
}
export default function CheckboxColumn(props: CheckboxColumnProps) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div className={classes.title}>{props.title}</div>
      <Divider className={classes.divider} />
      <div className={classes.checkboxes}>
        {props.checkboxes.map((cb, key) => (
          <div key={key + cb.label} id={key + cb.label}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={cb.value}
                  onChange={(e) => props.handleChange(key, e.target.checked)}
                  name={cb.label}
                  disabled={cb.disabled}
                  id={key + cb.label}
                />
              }
              label={cb.label}
            />
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginLeft: 30,
                height:
                  cb.subItems && cb.subItems.length > 0 && cb.value === true
                    ? cb.subItems.length * 42
                    : 0,
                overflow: "hidden",
                transition: "height 0.5s",
              }}
            >
              {cb.subItems?.map((scb, childKey) => (
                <FormControlLabel
                  key={childKey + cb.label + scb.label}
                  control={
                    <Checkbox
                      checked={scb.value}
                      onChange={(e) =>
                        props.handleChange(childKey, e.target.checked, key)
                      }
                      name={scb.label}
                      disabled={scb.disabled}
                      id={childKey + cb.label + scb.label}
                    />
                  }
                  label={scb.label}
                />
              ))}
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexDirection: "column",
    marginLeft: 50,
  },
  checkboxes: {
    display: "flex",
    flexDirection: "column",
  },
  title: {
    fontSize: 18,
    fontWeight: 600,
    margin: "15px 0",
  },
  divider: {
    marginBottom: 10,
  },
}));

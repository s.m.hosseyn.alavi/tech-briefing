import Header from "../../Components/Layout/Header";
import { makeStyles } from "@material-ui/core/styles";
import { Box, Button, Divider, Grid, Snackbar } from "@material-ui/core";
import CheckboxColumn from "./CheckboxColumn";
import { Dispatch, SetStateAction, useState } from "react";
import { States } from "../../Store/combine";
import { connect, ConnectedProps } from "react-redux";
import { update } from "../../Store/actions/updateResponse/update";
import MuiAlert from "@material-ui/lab/Alert";

export type CheckBox = {
  label: string;
  value: boolean;
  disabled?: boolean;
  subItems?: { label: string; value: boolean; disabled?: boolean }[];
};

const mapStateToProps = (state: States) => {
  return {
    current: state.reducer.updateResponse,
  };
};
const connector = connect(mapStateToProps, { update });

type HomeProps = ConnectedProps<typeof connector>;

function Home(props: HomeProps) {
  const [produkt, setProdukt] = useState<CheckBox[]>(initialProdukt);
  const [webeart, setWebeart] = useState(initialWebeart);
  const [format, setFormat] = useState(initialFormat);
  const [kommunikations, setKommunikations] = useState<CheckBox[]>(
    initialKommunikations
  );
  const [laufzeit, setLaufzeit] = useState(initialLaufzeit);
  const [preis, setPreis] = useState(initialPreis);
  const [snackbar, setSnackbar] = useState<{
    open: boolean;
  }>({
    open: false,
  });

  const classes = useStyles();

  function handleChangeCheckbox(
    key: number,
    value: boolean,
    changeState: Dispatch<SetStateAction<CheckBox[]>>,
    data: CheckBox[],
    parentKey?: number
  ) {
    let list = data;
    if (parentKey) {
      let childList = data[parentKey].subItems;
      childList &&
        childList.splice(key, 1, {
          ...childList[key],
          value: value,
        });
      list.splice(parentKey, 1, {
        ...list[parentKey],
        subItems: childList,
      });
      changeState([...list]);
    } else {
      list.splice(key, 1, {
        ...list[key],
        value: value,
      });
      changeState([...list]);
    }
  }

  function handleClear() {
    setProdukt([...initialProdukt]);
    setWebeart([...initialWebeart]);
    setFormat([...initialFormat]);
    setKommunikations([...initialKommunikations]);
    setLaufzeit([...initialLaufzeit]);
    setPreis([...initialPreis]);
  }

  async function handleUpdate() {
    try {
      await props.update([
        { title: "produkt", items: produkt },
        { title: "webeart", items: webeart },
        { title: "format", items: format },
        { title: "kommunikations", items: kommunikations },
        { title: "laufzeit", items: laufzeit },
        { title: "preis", items: preis },
      ]);
    } catch (error) {
      setSnackbar({
        open: true,
      });
    }

    setSnackbar({
      open: true,
    });
  }

  return (
    <div className={classes.root}>
      <Header />
      <Grid container className={classes.content}>
        <Grid item md={6} sm={12}>
          <Grid container>
            <Grid item md={6} sm={6} xs={12}>
              <CheckboxColumn
                title="Produkt"
                checkboxes={produkt}
                handleChange={(
                  key: number,
                  value: boolean,
                  parentKey?: number
                ) =>
                  handleChangeCheckbox(
                    key,
                    value,
                    setProdukt,
                    produkt,
                    parentKey
                  )
                }
              />
              <CheckboxColumn
                title="Webeart"
                checkboxes={webeart}
                handleChange={(
                  key: number,
                  value: boolean,
                  parentKey?: number
                ) =>
                  handleChangeCheckbox(
                    key,
                    value,
                    setWebeart,
                    webeart,
                    parentKey
                  )
                }
              />
            </Grid>
            <Grid item md={6} sm={6} xs={12}>
              <CheckboxColumn
                title="Format"
                checkboxes={format}
                handleChange={(
                  key: number,
                  value: boolean,
                  parentKey?: number
                ) =>
                  handleChangeCheckbox(key, value, setFormat, format, parentKey)
                }
              />
            </Grid>
          </Grid>
        </Grid>
        <Grid item md={6} sm={12}>
          <Grid container>
            <Grid item md={6} sm={6} xs={12}>
              <CheckboxColumn
                title="Kommunikations"
                checkboxes={kommunikations}
                handleChange={(
                  key: number,
                  value: boolean,
                  parentKey?: number
                ) =>
                  handleChangeCheckbox(
                    key,
                    value,
                    setKommunikations,
                    kommunikations,
                    parentKey
                  )
                }
              />
              <CheckboxColumn
                title="Laufzeit"
                checkboxes={laufzeit}
                handleChange={(
                  key: number,
                  value: boolean,
                  parentKey?: number
                ) =>
                  handleChangeCheckbox(
                    key,
                    value,
                    setLaufzeit,
                    laufzeit,
                    parentKey
                  )
                }
              />
            </Grid>
            <Grid item md={6} sm={6} xs={12}>
              <CheckboxColumn
                title="Preis"
                checkboxes={preis}
                handleChange={(
                  key: number,
                  value: boolean,
                  parentKey?: number
                ) =>
                  handleChangeCheckbox(key, value, setPreis, preis, parentKey)
                }
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Divider className={classes.divider} />
      <Box
        display="flex"
        flexDirection="row"
        justifyContent="space-between"
        width="100%"
      >
        <Button
          className={classes.button}
          color="secondary"
          onClick={() => handleClear()}
        >
          Clear
        </Button>
        <Button
          variant="contained"
          color="secondary"
          className={classes.button}
          onClick={() => handleUpdate()}
        >
          Update
        </Button>
      </Box>
      <Snackbar
        open={snackbar.open}
        autoHideDuration={6000}
        onClose={() =>
          setSnackbar({
            open: false,
          })
        }
      >
        <MuiAlert
          onClose={() =>
            setSnackbar({
              open: false,
            })
          }
          severity={props.current.status === 200 ? "success" : "error"}
          elevation={6}
          variant="filled"
        >
          {props.current.message}
        </MuiAlert>
      </Snackbar>
    </div>
  );
}

export default connector(Home);

const useStyles = makeStyles(() => ({
  root: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  content: {
    maxWidth: 1270,
    height: "calc(100vh - 161px)",
    overflowY: "auto",
  },
  button: {
    textTransform: "none",
    margin: 20,
    width: 100,
  },
  divider: {
    width: "100%",
  },
}));

const initialProdukt = [
  { label: "Plakat", value: false },
  { label: "Screens", value: false },
  { label: "MegaPoster", value: false },
  { label: "Speziollösungen", value: false },
  { label: "Innenformate", value: false },
  { label: "Aussenformate", value: false },
  { label: "Mobile", value: false },
  { label: "Promotionsflächen", value: false },
  { label: "Interaktionen", value: false },
];

const initialWebeart = [
  { label: "Analog", value: false },
  { label: "Digital", value: false },
];

const initialFormat = [
  { label: "F4", value: false },
  { label: "F200", value: false },
  { label: "F12", value: false },
  { label: "F24", value: false },
  { label: "ePanel", value: false },
  { label: "eBoard", value: false },
  { label: "HC, HCD", value: false },
  { label: "RailPoster", value: false },
  { label: "MegaPoster", value: false },
  { label: "Promotionsflöchen", value: false },
  { label: "Speziollösungen", value: false },
  { label: "Mobiletargeting", value: false },
  { label: "Timeinfo", value: false },
  { label: "Pylon Poster", value: false },
];

const initialKommunikations = [
  { label: "Strassen & Plätze", value: false, disabled: true },
  { label: "Bahnhöfe", value: false, disabled: true },
  {
    label: "Flughafen",
    value: false,
    subItems: [
      { label: "Item 1", value: false },
      { label: "Item 2", value: false },
      { label: "Item 3", value: false },
      { label: "Item 4", value: false },
    ],
  },
  { label: "Verkehrsmittel", value: false, disabled: true },
  { label: "POI/POS", value: false, disabled: true },
  { label: "Berge", value: false, disabled: true },
];

const initialLaufzeit = [
  { label: "1 Tag", value: false },
  { label: "7 Tag", value: false },
  { label: "14 Tag", value: false },
  { label: "21 Tag", value: false },
  { label: "1 Montat", value: false },
];

const initialPreis = [
  { label: "100-199", value: false },
  { label: "200-299", value: false },
  { label: "300-399", value: false },
  { label: "400-499", value: false },
  { label: "500+", value: false },
];

import logo from "../../../assets/logo.png";
import { makeStyles } from "@material-ui/core/styles";
import { Divider } from "@material-ui/core";

export default function Header() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <img src={logo} alt="logo" className={classes.logo} />
      <Divider className={classes.divider} />
    </div>
  );
}

const useStyles = makeStyles(() => ({
  root: {
    width: "100%",
    display: "flex",
    flexDirection: "column",
  },
  divider: {
    width: "100%",
    height: 2,
    backgroundColor: "#B2B2B2",
  },
  logo: {
    width: 150,
    margin: 10,
  },
}));

import { createStore, applyMiddleware } from "redux";
import Thunk from "redux-thunk";
import { Combine } from "./combine";

const store = createStore(Combine, applyMiddleware(Thunk));

export default store;

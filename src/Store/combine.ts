import { combineReducers } from "redux";
import reducer from "./reducer";

export const Combine = combineReducers({
  reducer,
});

export type States = ReturnType<typeof Combine>;

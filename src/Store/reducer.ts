import { ReducerActionType } from "./actions/updateResponse/allInterfaces";
import * as Types from "../Store/types/types";

export type UpdateResponse = {
  status: number;
  message: string;
};

export interface InitialInterface {
  updateResponse: UpdateResponse;
}

const initialState: InitialInterface = {
  updateResponse: {
    status: 0,
    message: "",
  },
};

const reducer = (
  state = initialState,
  actions: ReducerActionType
): InitialInterface => {
  switch (actions.type) {
    case Types.UPDATE_RESPONSE:
      return {
        ...state,
        updateResponse: actions.payload,
      };
  }
  return state;
};

export default reducer;

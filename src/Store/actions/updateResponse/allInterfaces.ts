import { UpdateActionInterface } from "./update";

export type ReducerActionType = UpdateActionInterface;

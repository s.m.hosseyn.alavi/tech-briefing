import { CheckBox } from "../../../Containers/Home";
import { UpdateResponse } from "../../reducer";
import * as Types from "../../types/types";
import { ActionType } from "../__thunkConfig";
import axios from "axios";

export interface UpdateActionInterface {
  type: string;
  payload: UpdateResponse;
}

type Checkboxes = {
  title: string;
  items: CheckBox[];
};
export const update = (value: Checkboxes[]): ActionType => async (dispatch) => {
  let query = "";
  value.map((item) => {
    item.items.map((i) => {
      query = query + `${i.label.replace(/ /g, "_")}=${i.value}&`;
      if (i.subItems) {
        i.subItems.map(
          (si) =>
            (query = query + `${si.label.replace(/ /g, "_")}=${si.value}&`)
        );
      }
      return i;
    });
    return item;
  });
  try {
    await axios.get("/format?" + query);
  } catch (error) {
    console.error("error in update", error);

    //fake response
    dispatch({
      type: Types.UPDATE_RESPONSE,
      payload: {
        status: 200,
        message: "update successfully ",
      },
    });
  }
};

import { Action } from "redux";
import { ThunkAction } from "redux-thunk";
import { States } from "../combine";

export type ActionType = ThunkAction<void, States, null, Action<string>>;
